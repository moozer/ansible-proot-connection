#!/usr/bin/env bash

# exit if anything fails.
set -e

DL_NAME="raspbian_lite_latest"
IMAGE_ZIP="raspberian_lite.zip"
IMAGE_URL="https://downloads.raspberrypi.org/$DL_NAME"
MNTTMP="./mnttmp"
TGZ_FILE="../raspberry_root_nodev.tgz"

if [ "x" == "x$1" ]; then
  WORKDIR="./"
else
  WORKDIR=$1
fi

BINDIR=$(dirname $(readlink -f $0))
mkdir -p $WORKDIR
cd $WORKDIR

echo "Downloading from $IMAGE_URL to $IMAGE_ZIP (if updated)"
wget -nv -N $IMAGE_URL

echo hard linking $DL_NAME TO $IMAGE_ZIP
if [ -f $IMAGE_ZIP ]; then rm $IMAGE_ZIP; fi
ln $DL_NAME $IMAGE_ZIP

IMAGE=$(unzip -l $IMAGE_ZIP | grep .img | cut -d ' ' -f 7)
if [ -f $IMAGE ]; then rm $IMAGE; fi
echo "Using image file $IMAGE"
unzip $IMAGE_ZIP

OFFSET=`echo $(/sbin/fdisk -l $IMAGE | tail -n 1 | cut -c40-49)`
echo "Splitting $image at offset $OFFSET"
dd if=$IMAGE of=part1.img bs=512 count=$OFFSET
dd if=$IMAGE of=part2.img skip=$OFFSET bs=512
ls -alh part?.img $IMAGE

echo "fuse mounting on $MNTTMP"
mkdir -p $MNTTMP
fuse-ext2 part2.img $MNTTMP

echo create tgz file for use in tests
tar -czC $MNTTMP -f $TGZ_FILE --exclude 'dev' ./

# unmounting
fusermount -u $MNTTMP
