#!/usr/bin/env bash

set -e

MNTDIR=mnt
TGZ_FILE=raspberry_root_nodev.tgz

echo extracting test data to $MNTDIR
mkdir -p $MNTDIR
tar -xzC $MNTDIR -f $TGZ_FILE

echo creating inventory file
echo $MNTDIR > inventory
